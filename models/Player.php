<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%player}}".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $nationality
 * @property string $birthday
 * @property integer $team_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CompetitionMatchPlayer[] $competitionMatchPlayers
 * @property Team $team
 */
class Player extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge([
            TimestampBehavior::className(),
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%player}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'nationality', 'birthday', 'team_id'], 'required'],
            [['birthday'], 'safe'],
            [['team_id', 'created_at', 'updated_at'], 'integer'],
            [['firstname', 'lastname'], 'string', 'max' => 100],
            [['nationality'], 'string', 'max' => 150],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'nationality' => Yii::t('app', 'Nationality'),
            'birthday' => Yii::t('app', 'Birthday'),
            'team_id' => Yii::t('app', 'Team ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatchPlayers()
    {
        return $this->hasMany(CompetitionMatchPlayer::className(), ['player_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }


    /**
     * @param $team_id
     * @return array
     * @internal param $competition_id
     */
    public static function forDepDrop($team_id)
    {
        $out = [];
        $players = Player::findAll(['team_id' => $team_id]);

        if (!empty($players)) {
            foreach ($players as $player) {
                $out[] = ['id' => $player->id, 'name' => $player->firstname . ' ' . $player->lastname];
            }
        }
        return $out;
    }
}

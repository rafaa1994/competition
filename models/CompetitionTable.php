<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%competition_table}}".
 *
 * @property integer $id
 * @property integer $competition_id
 * @property integer $competition_team_id
 * @property integer $win
 * @property integer $lose
 *
 * @property Competition $competition
 * @property CompetitionTeam $competitionTeam
 */
class CompetitionTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%competition_table}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competition_id', 'competition_team_id'], 'required'],
            [['competition_id', 'competition_team_id', 'win', 'lose'], 'integer'],
            [['competition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Competition::className(), 'targetAttribute' => ['competition_id' => 'id']],
            [['competition_team_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompetitionTeam::className(), 'targetAttribute' => ['competition_team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'competition_id' => Yii::t('app', 'Competition ID'),
            'competition_team_id' => Yii::t('app', 'Competition Team ID'),
            'win' => Yii::t('app', 'Win'),
            'lose' => Yii::t('app', 'Lose'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetition()
    {
        return $this->hasOne(Competition::className(), ['id' => 'competition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTeam()
    {
        return $this->hasOne(CompetitionTeam::className(), ['id' => 'competition_team_id']);
    }
}

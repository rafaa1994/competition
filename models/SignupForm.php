<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SignupForm extends Model
{
    /** @var string $firstname */
    public $firstname;
    /** @var string $lastname */
    public $lastname;
    /** @var string $email */
    public $email;
    /** @var string $password */
    public $password;
    /** @var int $role */
    public $role;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'password'], 'required'],
            ['email', 'email'],
            [['firstname', 'lastname', 'email', 'password'], 'string', 'max' => 100],
            ['role', 'integer']
        ];
    }


    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }
        $user = new User;

        $user->setAttributes([
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'email' => $this->email,
            'password_hash' => Yii::$app->security->generatePasswordHash($this->password),
            'role' => $this->role,
        ]);


        $result = $user->save();

        return $result;
    }

}
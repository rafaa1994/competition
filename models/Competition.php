<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%competition}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $date_from
 * @property integer $date_to
 *
 * @property CompetitionTeam[] $competitionTeams
 * @property CompetitionMatch[] $competitionMatches
 * @property CompetitionTable[] $competitionTables
 */
class Competition extends \yii\db\ActiveRecord
{

    public $dateTimeStart;
    public $dateTimeEnd;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%competition}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'unique'],
            [['name', 'dateTimeStart', 'dateTimeEnd'], 'required'],
            [['dateTimeStart', 'dateTimeEnd'], function ($attribute, $params, $validator) {
                if (!empty($this->dateTimeStart) && !empty($this->dateTimeEnd) && (Yii::$app->formatter->asTimestamp($this->dateTimeStart) > Yii::$app->formatter->asTimestamp($this->dateTimeEnd))) {
                    $this->addError($attribute, 'Date end cannot be lesser than date start.');
                }
            }],
            [['date_from', 'date_to'], 'integer'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->dateTimeEnd = Yii::$app->formatter->asDatetime($this->date_to, 'php:yy-m-d G:i');
        $this->dateTimeStart = Yii::$app->formatter->asDatetime($this->date_from, 'php:yy-m-d G:i');
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date_from = (int)Yii::$app->formatter->asTimestamp($this->dateTimeStart);
            $this->date_to = (int)Yii::$app->formatter->asTimestamp($this->dateTimeEnd);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTeams()
    {
        return $this->hasMany(CompetitionTeam::className(), ['competition_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatches()
    {
        return $this->hasMany(CompetitionMatch::className(), ['competition_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTables()
    {
        return $this->hasMany(CompetitionTable::className(), ['competition_id' => 'id']);
    }

    public function create()
    {
        if (!$this->validate()) {
            return false;
        }
        return $this->save();
    }

    public function teamHtml()
    {
        $html = '';
        foreach ($this->competitionTeams as $competitionTeam) {
            $field = !$competitionTeam->hasMatch() ? Html::a('<span class="glyphicon glyphicon-remove"></span>',
                ['competition/team-delete', 'id' => $competitionTeam->id], [
                    'class' => 'button-delete-attribute',
                    'title' => Yii::t('app', 'Delete'),
                ]) : '';
            $html .= Html::tag('li', $competitionTeam->team->name . ' ' . $field);
        }

        return Html::tag('ul', $html);
    }

}

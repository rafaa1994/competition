<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%competition_match}}".
 *
 * @property integer $id
 * @property integer $competition_id
 * @property integer $competition_team_1_id
 * @property integer $competition_team_2_id
 * @property integer $team_1_result
 * @property integer $team_2_result
 * @property integer $date
 *
 * @property Competition $competition
 * @property CompetitionTeam $competitionTeam1
 * @property CompetitionTeam $competitionTeam2
 * @property CompetitionMatchResult[] $competitionMatchResults
 * @property CompetitionMatchPlayer[] $competitionMatchPlayers
 * @property CompetitionMatchPlayer[] $competitionMatchTeam1Players
 * @property CompetitionMatchPlayer[] $competitionMatchTeam2Players
 */
class CompetitionMatch extends \yii\db\ActiveRecord
{

    public $playersTeam1 = [];
    public $playersTeam2 = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%competition_match}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competition_id', 'competition_team_1_id', 'competition_team_2_id', 'playersTeam1', 'playersTeam2'], 'required'],
            [['competition_team_1_id', 'competition_team_2_id'], function ($attribute, $params, $validator) {
                if ($this->competition_team_1_id == $this->competition_team_2_id) {
                    $this->addError($attribute, 'Teams are the same.');
                }
            }],
            [['playersTeam1', 'playersTeam2'], function ($attribute, $params, $validator) {
                if (sizeof($this->$attribute) != 6) {
                    $this->addError($attribute, 'Team must have 6 players.');
                }
            }],
            [['competition_id', 'competition_team_1_id', 'competition_team_2_id', 'team_1_result', 'team_2_result', 'date'], 'integer'],
            [['competition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Competition::className(), 'targetAttribute' => ['competition_id' => 'id']],
            [['competition_team_1_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompetitionTeam::className(), 'targetAttribute' => ['competition_team_1_id' => 'id']],
            [['competition_team_2_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompetitionTeam::className(), 'targetAttribute' => ['competition_team_2_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'competition_id' => Yii::t('app', 'Competition ID'),
            'competition_team_1_id' => Yii::t('app', 'Competition Team 1 ID'),
            'competition_team_2_id' => Yii::t('app', 'Competition Team 2 ID'),
            'team_1_result' => Yii::t('app', 'Team 1 Result'),
            'team_2_result' => Yii::t('app', 'Team 2 Result'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetition()
    {
        return $this->hasOne(Competition::className(), ['id' => 'competition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTeam1()
    {
        return $this->hasOne(CompetitionTeam::className(), ['id' => 'competition_team_1_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTeam2()
    {
        return $this->hasOne(CompetitionTeam::className(), ['id' => 'competition_team_2_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatchPlayers()
    {
        return $this->hasMany(CompetitionMatchPlayer::className(), ['competition_match_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatchTeam1Players()
    {
        return $this->hasMany(CompetitionMatchPlayer::className(), ['competition_match_id' => 'id'])->onCondition(['competition_team_id' => $this->competition_team_1_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatchTeam2Players()
    {
        return $this->hasMany(CompetitionMatchPlayer::className(), ['competition_match_id' => 'id'])->onCondition(['competition_team_id' => $this->competition_team_2_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatchResults()
    {
        return $this->hasMany(CompetitionMatchResult::className(), ['competition_match_id' => 'id']);
    }

    /**
     * @return bool|int
     */
    public function saveMatch()
    {
        if (!$this->save()) {
            return false;
        }

        $batchInsert = [];
        foreach ($this->playersTeam1 as $player) {
            $batchInsert[] = [
                $this->id,
                $this->competition_team_1_id,
                $player
            ];
        }

        foreach ($this->playersTeam2 as $player) {
            $batchInsert[] = [
                $this->id,
                $this->competition_team_2_id,
                $player
            ];
        }

        $result = Yii::$app->db->createCommand()->batchInsert(CompetitionMatchPlayer::tableName(), [
            'competition_match_id',
            'competition_team_id',
            'player_id'
        ], $batchInsert)->execute();

        return $result;
    }


    public function getTeamNames()
    {
        return $this->competitionTeam1->team->name . ' vs. ' . $this->competitionTeam2->team->name;
    }

    public function getTeam1Squad()
    {
        $html = '';
        foreach ($this->competitionMatchTeam1Players as $i) {
            $html .= Html::tag('li', $i->player->firstname . ' ' . $i->player->lastname);
        }

        return Html::tag('ul', $html);
    }

    public function getTeam2Squad()
    {
        $html = '';
        foreach ($this->competitionMatchTeam2Players as $i) {
            $html .= Html::tag('li', $i->player->firstname . ' ' . $i->player->lastname);
        }

        return Html::tag('ul', $html);
    }

    public function getResult()
    {
        $results = $this->competitionMatchResults;
        if ($results) {
            $html = '( ' . $this->team_1_result . ' : ' . $this->team_2_result . ' )';
            $html .= Html::tag('li', $results[0]->set1 . ' : ' . $results[1]->set1);
            $html .= Html::tag('li', $results[0]->set2 . ' : ' . $results[1]->set2);
            $html .= Html::tag('li', $results[0]->set3 . ' : ' . $results[1]->set3);
            if (isset($results[0]->set4) && isset($results[1]->set4)) {
                $html .= Html::tag('li', $results[0]->set4 . ' : ' . $results[1]->set4);
            }
            if (isset($results[0]->set5) && isset($results[1]->set5)) {
                $html .= Html::tag('li', $results[0]->set5 . ' : ' . $results[1]->set5);
            }
            return Html::tag('ul', $html);
        }
        return null;
    }

}

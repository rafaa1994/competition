<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%competition_team}}".
 *
 * @property integer $id
 * @property integer $competition_id
 * @property integer $team_id
 *
 * @property CompetitionMatch[] $competitionMatches
 * @property CompetitionMatch[] $competitionMatches0
 * @property CompetitionMatchPlayer[] $competitionMatchPlayers
 * @property CompetitionTable[] $competitionTables
 * @property Team $team
 */
class CompetitionTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%competition_team}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['team_id', 'unique', 'targetAttribute' => ['team_id', 'competition_id']],
            [['competition_id', 'team_id'], 'required'],
            [['competition_id', 'team_id'], 'integer'],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'competition_id' => Yii::t('app', 'Competition ID'),
            'team_id' => Yii::t('app', 'Team ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatches()
    {
        return $this->hasMany(CompetitionMatch::className(), ['competition_team_1_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatches0()
    {
        return $this->hasMany(CompetitionMatch::className(), ['competition_team_2_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatchPlayers()
    {
        return $this->hasMany(CompetitionMatchPlayer::className(), ['competition_team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTables()
    {
        return $this->hasMany(CompetitionTable::className(), ['competition_team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @param $competition_id
     * @return array
     */
    public static function teamsForDepDrop($competition_id)
    {
        $out = [];
        $competitionsTeams = CompetitionTeam::findAll(['competition_id' => $competition_id]);

        if (!empty($competitionsTeams)) {
            foreach ($competitionsTeams as $competitionsTeam) {
                $out[] = ['id' => $competitionsTeam->team_id, 'name' => $competitionsTeam->team->name];
            }
        }
        return $out;
    }

    /**
     * @return bool
     */
    public function hasMatch()
    {
        return ($this->competitionMatches0 || $this->competitionMatches);
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\BadRequestHttpException;

class MatchResultForm extends Model
{

    /** @var CompetitionMatch $match */
    public $match;

    /** @var int $team1set1 */
    public $team1set1;
    /** @var int $team1set2 */
    public $team1set2;
    /** @var int $team1set3 */
    public $team1set3;
    /** @var int $team1set4 */
    public $team1set4;
    /** @var int $team1set5 */
    public $team1set5;
    /** @var int $team1set1 */
    public $team2set1;
    /** @var int $team2set2 */
    public $team2set2;
    /** @var int $team2set3 */
    public $team2set3;
    /** @var int $team2set4 */
    public $team2set4;
    /** @var int $team2set5 */
    public $team2set5;


    public function init()
    {
        if (!$this->match) {
            throw new BadRequestHttpException('Match is empty');
        }
    }

    public function rules()
    {
        return [
            [['team1set1', 'team1set2', 'team1set3', 'team2set1', 'team2set2', 'team2set3'], 'required'],
            [['team1set1', 'team1set2', 'team1set3', 'team1set4', 'team1set5', 'team2set1', 'team2set2', 'team2set3', 'team2set4', 'team2set5'], 'integer', 'max' => 22],
            [['team1set4', 'team1set5', 'team2set4', 'team2set5'], function ($attribute, $params, $validator) {
                if (!$this->team1set1 || !$this->team1set2 || !$this->team1set3 || !$this->team2set1 || !$this->team2set2 || !$this->team2set3) {
                    $this->addError($attribute, 'Fill basic sets result.');
                }
            }],
            [['team1set4', 'team2set4', 'team1set5', 'team2set5'], function ($attribute, $params, $validator) {
                if (($this->team1set1 < $this->team2set1) && ($this->team1set2 < $this->team2set2) && ($this->team1set3 < $this->team2set3) && $this->$attribute) {
                    $this->addError($attribute, 'You dont have any sets');
                } elseif (($this->team1set1 > $this->team2set1) && ($this->team1set2 > $this->team2set2) && ($this->team1set3 > $this->team2set3) && $this->$attribute) {
                    $this->addError($attribute, 'You dont have any sets');
                }
            }],
        ];
    }

    public function saveResult()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($this->wonSets()) {
            $batchInsert = [
                [
                    $this->match->id,
                    $this->match->competition_team_1_id,
                    $this->team1set1,
                    $this->team1set2,
                    $this->team1set3,
                    $this->team1set4,
                    $this->team1set5
                ],
                [
                    $this->match->id,
                    $this->match->competition_team_2_id,
                    $this->team2set1,
                    $this->team2set2,
                    $this->team2set3,
                    $this->team2set4,
                    $this->team2set5
                ]
            ];

            $result = Yii::$app->db->createCommand()->batchInsert(CompetitionMatchResult::tableName(), [
                'competition_match_id',
                'competition_team_id',
                'set1',
                'set2',
                'set3',
                'set4',
                'set5',
            ], $batchInsert)->execute();

            return $result;
        }
      return false;
    }

    protected function wonSets()
    {
        $result1 = 0;
        $result2 = 0;

        if ($this->team1set1 < $this->team2set1) {
            $result2 += 1;
        } else if ($this->team1set1 > $this->team2set1) {
            $result1 += 1;
        }

        if ($this->team1set2 < $this->team2set2) {
            $result2 += 1;
        } else if ($this->team1set2 > $this->team2set2) {
            $result1 += 1;
        }

        if ($this->team1set3 < $this->team2set3) {
            $result2 += 1;
        } else if ($this->team1set3 > $this->team2set3) {
            $result1 += 1;
        }

        if ($this->team1set4 < $this->team2set4) {
            $result2 += 1;
        } else if ($this->team1set4 > $this->team2set4) {
            $result1 += 1;
        }

        if ($this->team1set5 < $this->team2set5) {
            $result2 += 1;
        } else if ($this->team1set5 > $this->team2set5) {
            $result1 += 1;
        }

        $this->match->team_1_result = $result1;
        $this->match->team_2_result = $result2;

        return $this->match->save();
    }

}
<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%team}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property CompetitionTeam[] $competitionTeams
 * @property Player[] $players
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'unique'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public function teamPlayersHtml()
    {
        $html = '';
        foreach ($this->players as $player) {
            $html .= Html::tag('li', $player->firstname .' '. $player->lastname);
        }

        return Html::tag('ul', $html);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTeams()
    {
        return $this->hasMany(CompetitionTeam::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(Player::className(), ['team_id' => 'id']);
    }

    public function delete()
    {
        Player::deleteAll(['id' => ArrayHelper::getColumn($this->players, 'id')]);
        return parent::delete();
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%competition_match_result}}".
 *
 * @property integer $competition_match_id
 * @property integer $competition_team_id
 * @property integer $set1
 * @property integer $set2
 * @property integer $set3
 * @property integer $set4
 * @property integer $set5
 *
 * @property CompetitionMatch $competitionMatch
 * @property CompetitionTeam $competitionTeam
 */
class CompetitionMatchResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%competition_match_result}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competition_match_id', 'competition_team_id'], 'required'],
            [['competition_match_id', 'competition_team_id', 'set1', 'set2', 'set3', 'set4', 'set5'], 'integer'],
            [['competition_match_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompetitionMatch::className(), 'targetAttribute' => ['competition_match_id' => 'id']],
            [['competition_team_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompetitionTeam::className(), 'targetAttribute' => ['competition_team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'competition_match_id' => Yii::t('app', 'Competition Match ID'),
            'competition_team_id' => Yii::t('app', 'Competition Team ID'),
            'set1' => Yii::t('app', 'Set1'),
            'set2' => Yii::t('app', 'Set2'),
            'set3' => Yii::t('app', 'Set3'),
            'set4' => Yii::t('app', 'Set4'),
            'set5' => Yii::t('app', 'Set5'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatch()
    {
        return $this->hasOne(CompetitionMatch::className(), ['id' => 'competition_match_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTeam()
    {
        return $this->hasOne(CompetitionTeam::className(), ['id' => 'competition_team_id']);
    }
}

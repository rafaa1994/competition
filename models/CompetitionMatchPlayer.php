<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%competition_match_player}}".
 *
 * @property integer $id
 * @property integer $competition_team_id
 * @property integer $competition_match_id
 * @property integer $player_id
 *
 * @property CompetitionMatch $competitionMatch
 * @property CompetitionTeam $competitionTeam
 * @property Player $player
 */
class CompetitionMatchPlayer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%competition_match_player}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competition_team_id', 'competition_match_id', 'player_id'], 'required'],
            [['competition_team_id', 'competition_match_id', 'player_id'], 'integer'],
            [['competition_match_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompetitionMatch::className(), 'targetAttribute' => ['competition_match_id' => 'id']],
            [['competition_team_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompetitionTeam::className(), 'targetAttribute' => ['competition_team_id' => 'id']],
            [['player_id'], 'exist', 'skipOnError' => true, 'targetClass' => Player::className(), 'targetAttribute' => ['player_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'competition_team_id' => Yii::t('app', 'Competition Team ID'),
            'competition_match_id' => Yii::t('app', 'Competition Match ID'),
            'player_id' => Yii::t('app', 'Player ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionMatch()
    {
        return $this->hasOne(CompetitionMatch::className(), ['id' => 'competition_match_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetitionTeam()
    {
        return $this->hasOne(CompetitionTeam::className(), ['id' => 'competition_team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(Player::className(), ['id' => 'player_id']);
    }
}

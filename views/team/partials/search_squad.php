<?php

use yii\grid\GridView;
use yii\helpers\Html;

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        [
            'label' => 'Players',
            'format' => 'raw',
            'value' => function($model) {
            /** @var $model \app\models\Team*/
                return $model->teamPlayersHtml();
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model) {
                    return Yii::$app->user->identity->isAdmin()
                        ? Html::a('<span class="glyphicon glyphicon-remove"></span>',
                            ['team/delete', 'id' => $model->id], [
                                'id' => 'remove-attibute',
                                'class' => 'button-delete-attribute',
                                'title' => Yii::t('app', 'Delete'),
                            ]) : '';
                }
            ]
        ],
    ],
]); ?>

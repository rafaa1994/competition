<?php

use yii\grid\GridView;
use yii\helpers\Html;

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Yii::$app->user->identity->isAdmin()
                        ? Html::a('<span class="glyphicon glyphicon-pencil"> </span>',
                            ['team/index', 'id' => $model->id], [
                                'class' => 'button-attribute-update',
                                'title' => Yii::t('app', 'Update'),
                            ]) : '';
                },
                'delete' => function ($url, $model) {
                    return Yii::$app->user->identity->isAdmin()
                        ? Html::a('<span class="glyphicon glyphicon-remove"></span>',
                            ['team/delete', 'id' => $model->id], [
                                'id' => 'remove-attibute',
                                'class' => 'button-delete-attribute',
                                'title' => Yii::t('app', 'Delete'),
                            ]) : '';
                }
            ]
        ],
    ],
]); ?>

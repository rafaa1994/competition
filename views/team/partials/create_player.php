<?php

use kartik\datetime\DateTimePicker;
use kartik\widgets\DatePicker;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/** @var array $teams */

?>

<?php
$form = ActiveForm::begin([
    'id' => 'create-form',
]); ?>

<?= $form->field($model, 'firstname')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'lastname')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'nationality')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'birthday')->hint('Format RRRR-MM-DD')->widget(DatePicker::classname(), array(
    'type' => DatePicker::TYPE_INPUT,
    'pluginOptions' => array(
        'format' => 'yyyy-mm-dd')
)) ?>
<?= $form->field($model, 'team_id')->dropDownList(ArrayHelper::map($teams, 'id', 'name'))->label('Team') ?>


<div class="col-md-12">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'style' => ['width' =>'100%'] ,'name' => 'create-button']) ?>
</div>

<?php ActiveForm::end(); ?>

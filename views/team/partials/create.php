<?php

use kartik\datetime\DateTimePicker;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;


?>

<?php
$form = ActiveForm::begin([
    'id' => 'create-form',
]); ?>

<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
<div class="col-md-12">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'style' => ['width' =>'100%'] ,'name' => 'create-button']) ?>
</div>

<?php ActiveForm::end(); ?>

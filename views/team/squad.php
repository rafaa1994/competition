<?php

/** @var $this View */
/** @var $dataProvider \yii\data\ActiveDataProvider */
use yii\web\View;

?>

<?php if (Yii::$app->user->identity->isAdmin()) :?>
    <div class="col-md-5">
        <div id="create-new">
            <?= $this->render('partials/create_player', ['model' => $model, 'teams' => $dataProvider->models]) ?>
        </div>
    </div>
    <div class="col-md-7">
        <div class="table table-responsive">
            <?= $this->render('partials/search_squad', ['dataProvider' => $dataProvider]) ?>
        </div>
    </div>
<?php else:?>
    <div class="col-md-12">
        <div class="table table-responsive">
            <?= $this->render('partials/search_squad', ['dataProvider' => $dataProvider]) ?>
        </div>
    </div>
<?php endif;?>
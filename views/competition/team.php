<?php

/** @var $this View */
use yii\web\View;

?>

<?php if (Yii::$app->user->identity->isAdmin()) :?>
    <div class="col-md-5">
        <div id="create-new">
            <?= $this->render('partials/team_create', ['model' => $model, 'competitions' => $competitions]) ?>
        </div>
    </div>
    <div class="col-md-7">
        <div class="table table-responsive">
            <?= $this->render('partials/team_search', ['dataProvider' => $dataProvider]) ?>
        </div>
    </div>
<?php else:?>
    <div class="col-md-12">
        <div class="table table-responsive">
            <?= $this->render('partials/team_search', ['dataProvider' => $dataProvider]) ?>
        </div>
    </div>
<?php endif;?>
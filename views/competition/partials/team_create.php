<?php

use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php
$form = ActiveForm::begin([
    'id' => 'create-form',
]); ?>

<?= $form->field($model, 'competition_id')->dropDownList(ArrayHelper::map($competitions, 'id', 'name'), ['id' => 'parent', 'prompt' => 'Select ...'])->label('Competition') ?>

<?= $form->field($model, 'team_id')->widget(DepDrop::classname(), [
    'type' => DepDrop::TYPE_SELECT2,
    'data' => [],
    'options' => ['id' => 'sub', 'placeholder' => 'Select ...'],
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['parent'],
        'url' => Url::to(['/competition/available-team']),
    ]
])->label('Team');
?>

<div class="col-md-12">
    <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn btn-primary', 'style' => ['width' => '100%'], 'name' => 'create-button']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php

use yii\grid\GridView;
use yii\helpers\Html;
?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        [
            'label' => 'Teams',
            'format' => 'raw',
            'value' => function($model) {
                /** @var $model \app\models\Competition */
                return $model->teamHtml();
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Yii::$app->user->identity->isAdmin() && (int) Yii::$app->formatter->asTimestamp('now') > $model->date_from
                        ? Html::a('<span class="glyphicon glyphicon-pencil"> </span>',
                            ['competition/index', 'id' => $model->id], [
                                'class' => 'button-attribute-update',
                                'title' => Yii::t('app', 'Update'),
                            ]) : '';
                },
                'delete' => function ($url, $model) {
                    return Yii::$app->user->identity->isAdmin() && (int) Yii::$app->formatter->asTimestamp('now') > $model->date_from
                        ? Html::a('<span class="glyphicon glyphicon-remove"></span>',
                            ['competition/delete', 'id' => $model->id], [
                                'id' => 'remove-attibute',
                                'class' => 'button-delete-attribute',
                                'title' => Yii::t('app', 'Delete'),
                            ]) : '';
                }
            ]
        ],
    ],
]); ?>

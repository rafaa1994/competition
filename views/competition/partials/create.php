<?php

use kartik\datetime\DateTimePicker;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;


?>

<?php
$form = ActiveForm::begin([
    'id' => 'create-form',
]); ?>

<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'dateTimeStart')->hint('Format RRRR-MM-DD GG:MM')->widget(DateTimePicker::classname(), [
    'type' => DateTimePicker::TYPE_INPUT,
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd hh:ii']
]) ?>
<?= $form->field($model, 'dateTimeEnd')->hint('Format RRRR-MM-DD GG:MM')->widget(DateTimePicker::classname(), [
    'type' => DateTimePicker::TYPE_INPUT,
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd hh:ii']
]) ?>

<div class="col-md-12">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'style' => ['width' => '100%'], 'name' => 'create-button']) ?>
</div>

<?php ActiveForm::end(); ?>

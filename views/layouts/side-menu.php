<?php

use kartik\sidenav\SideNav;
use yii\helpers\Url;

/** @var \yii\web\View $this */

$type = SideNav::TYPE_INFO;
$item = Yii::$app->request->url;
?>

<?=
SideNav::widget([
    'type' => $type,
    'encodeLabels' => false,
    'items' => [
        ['label' => 'Home', 'url' => Url::to(['/site/index']), 'active' => ($item == '/site/index')],
        ['label' => 'Competitions', 'url' => Url::to(['/competition/index']), 'active' => ($item == 'competition/index')],
        ['label' => 'Teams', 'url' => Url::to(['/team/index']), 'active' => ($item == '/team/index')],
        ['label' => 'Teams Squads', 'url' => Url::to(['/team/squads']), 'active' => ($item == '/team/squads')],
        ['label' => 'Competitions Teams', 'url' => Url::to(['/competition/team']), 'active' => ($item == 'competition/team')],
        ['label' => 'Competitions Matches', 'url' => Url::to(['/competition-matches/index']), 'active' => ($item == 'competition-matches/index')],
    ]
]);
?>
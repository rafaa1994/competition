<?php

use app\models\CompetitionMatch;
use yii\grid\GridView;
use yii\helpers\Html;


?>

<div style="margin-top: 20px">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'label' => 'Competition',
                'attribute' => 'competition.name',
            ],
            [
                'label' => 'Teams',
                'value' => function ($model) {
                    /** @var $model CompetitionMatch */

                    return $model->getTeamNames();
                },
            ],
            [
                'label' => 'Squad Team 1',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var $model CompetitionMatch */

                    return $model->getTeam1Squad();
                },
            ],
            [
                'label' => 'Squad Team 2',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var $model CompetitionMatch */

                    return $model->getTeam2Squad();
                },
            ],
            [
                'label' => 'Result',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var $model CompetitionMatch */
                    return $model->getResult();
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        /** @var $model CompetitionMatch */
                        return Yii::$app->user->identity->isAdmin() && !$model->competitionMatchResults
                            ? Html::a('<span class="glyphicon glyphicon-flag"> </span>',
                                ['competition-matches/update', 'id' => $model->id], [
                                    'class' => 'button-attribute-update',
                                    'title' => Yii::t('app', 'Result'),
                                ]) : '';
                    },
                ]
            ],
        ],
    ]); ?>


</div>
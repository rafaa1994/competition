<?php

use kartik\widgets\DepDrop;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php
$form = ActiveForm::begin([
    'id' => 'create-form',
]); ?>

<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'competition_id')->dropDownList(ArrayHelper::map($competitions, 'id', 'name'), ['id' => 'competition', 'prompt' => 'Select ...'])->label('Competition') ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'competition_team_1_id')->widget(DepDrop::classname(), [
            'type' => DepDrop::TYPE_DEFAULT,
            'data' => [],
            'options' => ['id' => 'parent1', 'placeholder' => 'Select ...'],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['competition'],
                'url' => Url::to(['/competition-matches/available-teams']),
            ]
        ])->label('Team 1'); ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'competition_team_2_id')->widget(DepDrop::classname(), [
            'type' => DepDrop::TYPE_DEFAULT,
            'data' => [],
            'options' => ['id' => 'parent2', 'placeholder' => 'Select ...'],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['competition'],
                'url' => Url::to(['/competition-matches/available-teams']),
            ]
        ])->label('Team 2'); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'playersTeam1')->widget(DepDrop::classname(), [
            'type' => DepDrop::TYPE_SELECT2,
            'data' => [],
            'options' => ['id' => 'sub1', 'placeholder' => 'Select ...', 'multiple' => true],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['parent1'],
                'url' => Url::to(['/competition-matches/available-players']),
            ]
        ])->label('Squad 1'); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'playersTeam2')->widget(DepDrop::classname(), [
            'type' => DepDrop::TYPE_SELECT2,
            'data' => [],
            'options' => ['id' => 'sub2', 'placeholder' => 'Select ...' , 'multiple' => true],
            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
            'pluginOptions' => [
                'depends' => ['parent2'],
                'url' => Url::to(['/competition-matches/available-players']),
            ]
        ])->label('Squad 2'); ?>
    </div>
</div>
<div class="col-md-12">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary', 'style' => ['width' => '100%'], 'name' => 'create-button']) ?>
</div>
<?php ActiveForm::end(); ?>

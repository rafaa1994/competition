<?php

/** @var $this View */
use yii\web\View;

?>

<?php if (Yii::$app->user->identity->isAdmin()) : ?>
    <div class="col-md-12">
        <div id="create-new">
            <?= $this->render('partials/create', ['model' => $model, 'competitions' => $competitions, 'teams' => $teams]) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table table-responsive">
            <?= $this->render('partials/search', ['dataProvider' => $dataProvider]) ?>
        </div>
    </div>
<?php else: ?>
    <div class="col-md-12">
        <div class="table table-responsive">
            <?= $this->render('partials/search', ['dataProvider' => $dataProvider]) ?>
        </div>
    </div>
<?php endif; ?>
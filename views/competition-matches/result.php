<?php

/** @var $match CompetitionMatch */
use app\models\CompetitionMatch;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

?>


<div class="col-md-12">
    <div class="table table-responsive">
        <?= GridView::widget([
            'dataProvider' => (new \yii\data\ArrayDataProvider(['models' => CompetitionMatch::findAll([$match->id])])),
            'columns' => [
                [
                    'label' => 'Competition',
                    'attribute' => 'competition.name',
                ],
                [
                    'label' => 'Teams',
                    'value' => function ($model) {
                        /** @var $model CompetitionMatch */

                        return $model->getTeamNames();
                    },
                ],
                [
                    'label' => 'Squad Team 1',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var $model CompetitionMatch */

                        return $model->getTeam1Squad();
                    },
                ],
                [
                    'label' => 'Squad Team 2',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var $model CompetitionMatch */

                        return $model->getTeam2Squad();
                    },
                ],
            ],
        ]); ?>
    </div>
</div>
<div class="col-md-12">
    <div id="create-new">
    </div>
</div>
<?php if (Yii::$app->user->identity->isAdmin()) : ?>
    <div class="col-md-12">
        <?php
        $form = ActiveForm::begin([
            'id' => 'create-form',
        ]); ?>
            <div class="row">
                <div class="col-md-offset-2 col-md-4">
                    <?= $form->field($model, 'team1set1')->textInput()->label('Team 1 Set 1') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'team2set1')->textInput()->label('Team 2 Set 1') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-4">
                    <?= $form->field($model, 'team1set2')->textInput()->label('Team 1 Set 2') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'team2set2')->textInput()->label('Team 2 Set 2') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-4">
                    <?= $form->field($model, 'team1set3')->textInput()->label('Team 1 Set 3') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'team2set3')->textInput()->label('Team 2 Set 3') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-4">
                    <?= $form->field($model, 'team1set4')->textInput()->label('Team 1 Set 4') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'team2set4')->textInput()->label('Team 2 Set 4') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-4">
                    <?= $form->field($model, 'team1set5')->textInput()->label('Team 1 Set 5') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'team2set5')->textInput()->label('Team 2 Set 5') ?>
                </div>
            </div>
            <div class="col-md-offset-4 col-md-4">
                <?= Html::submitButton('Add', ['class' => 'btn btn-primary', 'style' => ['width' => '100%'], 'name' => 'create-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>

    </div>
<?php endif; ?>

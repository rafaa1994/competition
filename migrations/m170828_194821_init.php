<?php

use yii\db\Migration;

class m170828_194821_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(11),
            'firstname' => $this->string(100)->notNull(),
            'lastname' => $this->string(100)->notNull(),
            'email' => $this->string(100)->notNull(),
            'auth_key' => $this->string(255),
            'password_hash' => $this->string(255),
            'password_reset_token' => $this->string(255),
            'role' => $this->smallInteger(6)->defaultValue(1),
            'status' => $this->smallInteger(6)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11)
        ]);

        $this->createTable('{{%player}}', [
            'id' => $this->primaryKey(11),
            'firstname' => $this->string(100)->notNull(),
            'lastname' => $this->string(100)->notNull(),
            'nationality' => $this->string(150)->notNull(),
            'birthday' => $this->date()->notNull(),
            'team_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11)
        ]);

        $this->createTable('{{%team}}', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(100)->notNull()
        ]);

        $this->createTable('{{%competition}}', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(150)->notNull(),
            'date_from' => $this->integer(11)->notNull(),
            'date_to' => $this->integer(11)->notNull()
        ]);

        $this->createTable('{{%competition_team}}', [
            'id' => $this->primaryKey(11),
            'competition_id' => $this->integer(11)->notNull(),
            'team_id' => $this->integer(11)->notNull()
        ]);

        $this->createTable('{{%competition_match}}', [
            'id' => $this->primaryKey(11),
            'competition_id' => $this->integer(11)->notNull(),
            'competition_team_1_id' => $this->integer(11)->notNull(),
            'competition_team_2_id' => $this->integer(11)->notNull(),
            'team_1_result' => $this->integer(11),
            'team_2_result' => $this->integer(11),
            'date' => $this->integer(11)
        ]);

        $this->createTable('{{%competition_match_result}}', [
            'competition_match_id' => $this->integer(11)->notNull(),
            'competition_team_id' => $this->integer(11)->notNull(),
            'set1' => $this->integer(11),
            'set2' => $this->integer(11),
            'set3' => $this->integer(11),
            'set4' => $this->integer(11),
            'set5' => $this->integer(11),
        ]);

        $this->createTable('{{%competition_table}}', [
            'id' => $this->primaryKey(11),
            'competition_id' => $this->integer(11)->notNull(),
            'competition_team_id' => $this->integer(11)->notNull(),
            'win' => $this->integer(11)->defaultValue(0),
            'lose' => $this->integer(11)->defaultValue(0)
        ]);

        $this->createTable('{{%competition_match_player}}', [
            'id' => $this->primaryKey(11),
            'competition_team_id' => $this->integer(11)->notNull(),
            'competition_match_id' => $this->integer(11)->notNull(),
            'player_id' => $this->integer(11)->notNull()
        ]);

        $this->createIndex('player_idx_team', '{{%player}}', 'team_id');
        $this->createIndex('competition_match_idx_competition', '{{%competition_match}}', 'competition_id');
        $this->createIndex('competition_match_idx_team1', '{{%competition_match}}', 'competition_team_1_id');
        $this->createIndex('competition_match_idx_team2', '{{%competition_match}}', 'competition_team_2_id');
        $this->createIndex('competition_team_idx_competition', '{{%competition_team}}', 'competition_id');
        $this->createIndex('competition_team_idx_team', '{{%competition_team}}', 'team_id');
        $this->createIndex('competition_table_idx_competition', '{{%competition_table}}', 'competition_id');
        $this->createIndex('competition_table_idx_competition_team', '{{%competition_table}}', 'competition_team_id');
        $this->createIndex('competition_match_player_idx_competition_team', '{{%competition_match_player}}', 'competition_team_id');
        $this->createIndex('competition_match_player_idx_competition_match', '{{%competition_match_player}}', 'competition_match_id');
        $this->createIndex('competition_match_player_idx_player', '{{%competition_match_player}}', 'player_id');
        $this->createIndex('competition_match_result_idx_competition_match', '{{%competition_match_result}}', 'competition_match_id');
        $this->createIndex('competition_match_result_idx_competition_team', '{{%competition_match_result}}', 'competition_team_id');

        $this->addForeignKey('player_fk_team', 'player', 'team_id', 'team', 'id');

        $this->addForeignKey('competition_match_fk_competition', 'competition_match', 'competition_id', 'competition', 'id');
        $this->addForeignKey('competition_match_fk_team1', 'competition_match', 'competition_team_1_id', 'competition_team', 'id');
        $this->addForeignKey('competition_match_fk_team2', 'competition_match', 'competition_team_2_id', 'competition_team', 'id');
        $this->addForeignKey('competition_match_result_fk_competition_match', 'competition_match_result', 'competition_match_id', 'competition_match', 'id');
        $this->addForeignKey('competition_match_result_fk_competition_team', 'competition_match_result', 'competition_team_id', 'competition_team', 'id');

        $this->addForeignKey('competition_team_fk_team', 'competition_team', 'team_id', 'team', 'id');

        $this->addForeignKey('competition_table_fk_competition_team', 'competition_table', 'competition_team_id', 'competition_team', 'id');
        $this->addForeignKey('competition_table_fk_competition', 'competition_table', 'competition_id', 'competition', 'id');

        $this->addForeignKey('competition_match_player_fk_competition_team', 'competition_match_player', 'competition_team_id', 'competition_team', 'id');
        $this->addForeignKey('competition_match_player_fk_competition_match', 'competition_match_player', 'competition_match_id', 'competition_match', 'id');
        $this->addForeignKey('competition_match_player_fk_player', 'competition_match_player', 'player_id', 'player', 'id');

    }

    public function safeDown()
    {
        echo "m170828_194821_init cannot be reverted.\n";

        return false;
    }

}

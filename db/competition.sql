--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.8
-- Dumped by pg_dump version 9.5.8

-- Started on 2017-09-01 01:58:11 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12395)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2264 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 189 (class 1259 OID 17218)
-- Name: competition; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE competition (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    date_from integer NOT NULL,
    date_to integer NOT NULL
);


ALTER TABLE competition OWNER TO root;

--
-- TOC entry 188 (class 1259 OID 17216)
-- Name: competition_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE competition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE competition_id_seq OWNER TO root;

--
-- TOC entry 2265 (class 0 OID 0)
-- Dependencies: 188
-- Name: competition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE competition_id_seq OWNED BY competition.id;


--
-- TOC entry 193 (class 1259 OID 17234)
-- Name: competition_match; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE competition_match (
    id integer NOT NULL,
    competition_id integer NOT NULL,
    competition_team_1_id integer NOT NULL,
    competition_team_2_id integer NOT NULL,
    team_1_result integer,
    team_2_result integer,
    date integer
);


ALTER TABLE competition_match OWNER TO root;

--
-- TOC entry 192 (class 1259 OID 17232)
-- Name: competition_match_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE competition_match_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE competition_match_id_seq OWNER TO root;

--
-- TOC entry 2266 (class 0 OID 0)
-- Dependencies: 192
-- Name: competition_match_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE competition_match_id_seq OWNED BY competition_match.id;


--
-- TOC entry 198 (class 1259 OID 17255)
-- Name: competition_match_player; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE competition_match_player (
    id integer NOT NULL,
    competition_team_id integer NOT NULL,
    competition_match_id integer NOT NULL,
    player_id integer NOT NULL
);


ALTER TABLE competition_match_player OWNER TO root;

--
-- TOC entry 197 (class 1259 OID 17253)
-- Name: competition_match_player_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE competition_match_player_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE competition_match_player_id_seq OWNER TO root;

--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 197
-- Name: competition_match_player_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE competition_match_player_id_seq OWNED BY competition_match_player.id;


--
-- TOC entry 194 (class 1259 OID 17240)
-- Name: competition_match_result; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE competition_match_result (
    competition_match_id integer NOT NULL,
    competition_team_id integer NOT NULL,
    set1 integer,
    set2 integer,
    set3 integer,
    set4 integer,
    set5 integer
);


ALTER TABLE competition_match_result OWNER TO root;

--
-- TOC entry 196 (class 1259 OID 17245)
-- Name: competition_table; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE competition_table (
    id integer NOT NULL,
    competition_id integer NOT NULL,
    competition_team_id integer NOT NULL,
    win integer DEFAULT 0,
    lose integer DEFAULT 0
);


ALTER TABLE competition_table OWNER TO root;

--
-- TOC entry 195 (class 1259 OID 17243)
-- Name: competition_table_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE competition_table_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE competition_table_id_seq OWNER TO root;

--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 195
-- Name: competition_table_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE competition_table_id_seq OWNED BY competition_table.id;


--
-- TOC entry 191 (class 1259 OID 17226)
-- Name: competition_team; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE competition_team (
    id integer NOT NULL,
    competition_id integer NOT NULL,
    team_id integer NOT NULL
);


ALTER TABLE competition_team OWNER TO root;

--
-- TOC entry 190 (class 1259 OID 17224)
-- Name: competition_team_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE competition_team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE competition_team_id_seq OWNER TO root;

--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 190
-- Name: competition_team_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE competition_team_id_seq OWNED BY competition_team.id;


--
-- TOC entry 181 (class 1259 OID 17182)
-- Name: migration; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE migration (
    version character varying(180) NOT NULL,
    apply_time integer
);


ALTER TABLE migration OWNER TO root;

--
-- TOC entry 185 (class 1259 OID 17202)
-- Name: player; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE player (
    id integer NOT NULL,
    firstname character varying(100) NOT NULL,
    lastname character varying(100) NOT NULL,
    nationality character varying(150) NOT NULL,
    birthday date NOT NULL,
    team_id integer NOT NULL,
    created_at integer,
    updated_at integer
);


ALTER TABLE player OWNER TO root;

--
-- TOC entry 184 (class 1259 OID 17200)
-- Name: player_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE player_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE player_id_seq OWNER TO root;

--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 184
-- Name: player_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE player_id_seq OWNED BY player.id;


--
-- TOC entry 187 (class 1259 OID 17210)
-- Name: team; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE team (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE team OWNER TO root;

--
-- TOC entry 186 (class 1259 OID 17208)
-- Name: team_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE team_id_seq OWNER TO root;

--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 186
-- Name: team_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE team_id_seq OWNED BY team.id;


--
-- TOC entry 183 (class 1259 OID 17189)
-- Name: user; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "user" (
    id integer NOT NULL,
    firstname character varying(100) NOT NULL,
    lastname character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    auth_key character varying(255),
    password_hash character varying(255),
    password_reset_token character varying(255),
    role smallint DEFAULT 1,
    status smallint DEFAULT 1,
    created_at integer,
    updated_at integer
);


ALTER TABLE "user" OWNER TO root;

--
-- TOC entry 182 (class 1259 OID 17187)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO root;

--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 182
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- TOC entry 2075 (class 2604 OID 17221)
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition ALTER COLUMN id SET DEFAULT nextval('competition_id_seq'::regclass);


--
-- TOC entry 2077 (class 2604 OID 17237)
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match ALTER COLUMN id SET DEFAULT nextval('competition_match_id_seq'::regclass);


--
-- TOC entry 2081 (class 2604 OID 17258)
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match_player ALTER COLUMN id SET DEFAULT nextval('competition_match_player_id_seq'::regclass);


--
-- TOC entry 2078 (class 2604 OID 17248)
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_table ALTER COLUMN id SET DEFAULT nextval('competition_table_id_seq'::regclass);


--
-- TOC entry 2076 (class 2604 OID 17229)
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_team ALTER COLUMN id SET DEFAULT nextval('competition_team_id_seq'::regclass);


--
-- TOC entry 2073 (class 2604 OID 17205)
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY player ALTER COLUMN id SET DEFAULT nextval('player_id_seq'::regclass);


--
-- TOC entry 2074 (class 2604 OID 17213)
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY team ALTER COLUMN id SET DEFAULT nextval('team_id_seq'::regclass);


--
-- TOC entry 2070 (class 2604 OID 17192)
-- Name: id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 2247 (class 0 OID 17218)
-- Dependencies: 189
-- Data for Name: competition; Type: TABLE DATA; Schema: public; Owner: root
--

COPY competition (id, name, date_from, date_to) FROM stdin;
\.


--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 188
-- Name: competition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('competition_id_seq', 1, false);


--
-- TOC entry 2251 (class 0 OID 17234)
-- Dependencies: 193
-- Data for Name: competition_match; Type: TABLE DATA; Schema: public; Owner: root
--

COPY competition_match (id, competition_id, competition_team_1_id, competition_team_2_id, team_1_result, team_2_result, date) FROM stdin;
\.


--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 192
-- Name: competition_match_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('competition_match_id_seq', 1, false);


--
-- TOC entry 2256 (class 0 OID 17255)
-- Dependencies: 198
-- Data for Name: competition_match_player; Type: TABLE DATA; Schema: public; Owner: root
--

COPY competition_match_player (id, competition_team_id, competition_match_id, player_id) FROM stdin;
\.


--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 197
-- Name: competition_match_player_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('competition_match_player_id_seq', 1, false);


--
-- TOC entry 2252 (class 0 OID 17240)
-- Dependencies: 194
-- Data for Name: competition_match_result; Type: TABLE DATA; Schema: public; Owner: root
--

COPY competition_match_result (competition_match_id, competition_team_id, set1, set2, set3, set4, set5) FROM stdin;
\.


--
-- TOC entry 2254 (class 0 OID 17245)
-- Dependencies: 196
-- Data for Name: competition_table; Type: TABLE DATA; Schema: public; Owner: root
--

COPY competition_table (id, competition_id, competition_team_id, win, lose) FROM stdin;
\.


--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 195
-- Name: competition_table_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('competition_table_id_seq', 1, false);


--
-- TOC entry 2249 (class 0 OID 17226)
-- Dependencies: 191
-- Data for Name: competition_team; Type: TABLE DATA; Schema: public; Owner: root
--

COPY competition_team (id, competition_id, team_id) FROM stdin;
\.


--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 190
-- Name: competition_team_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('competition_team_id_seq', 1, false);


--
-- TOC entry 2239 (class 0 OID 17182)
-- Dependencies: 181
-- Data for Name: migration; Type: TABLE DATA; Schema: public; Owner: root
--

COPY migration (version, apply_time) FROM stdin;
m000000_000000_base	1504223830
m170828_194821_init	1504223831
\.


--
-- TOC entry 2243 (class 0 OID 17202)
-- Dependencies: 185
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: root
--

COPY player (id, firstname, lastname, nationality, birthday, team_id, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 184
-- Name: player_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('player_id_seq', 1, false);


--
-- TOC entry 2245 (class 0 OID 17210)
-- Dependencies: 187
-- Data for Name: team; Type: TABLE DATA; Schema: public; Owner: root
--

COPY team (id, name) FROM stdin;
\.


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 186
-- Name: team_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('team_id_seq', 1, false);


--
-- TOC entry 2241 (class 0 OID 17189)
-- Dependencies: 183
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "user" (id, firstname, lastname, email, auth_key, password_hash, password_reset_token, role, status, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 182
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('user_id_seq', 1, false);


--
-- TOC entry 2101 (class 2606 OID 17239)
-- Name: competition_match_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match
    ADD CONSTRAINT competition_match_pkey PRIMARY KEY (id);


--
-- TOC entry 2112 (class 2606 OID 17260)
-- Name: competition_match_player_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match_player
    ADD CONSTRAINT competition_match_player_pkey PRIMARY KEY (id);


--
-- TOC entry 2092 (class 2606 OID 17223)
-- Name: competition_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition
    ADD CONSTRAINT competition_pkey PRIMARY KEY (id);


--
-- TOC entry 2107 (class 2606 OID 17252)
-- Name: competition_table_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_table
    ADD CONSTRAINT competition_table_pkey PRIMARY KEY (id);


--
-- TOC entry 2096 (class 2606 OID 17231)
-- Name: competition_team_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_team
    ADD CONSTRAINT competition_team_pkey PRIMARY KEY (id);


--
-- TOC entry 2083 (class 2606 OID 17186)
-- Name: migration_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (version);


--
-- TOC entry 2088 (class 2606 OID 17207)
-- Name: player_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY player
    ADD CONSTRAINT player_pkey PRIMARY KEY (id);


--
-- TOC entry 2090 (class 2606 OID 17215)
-- Name: team_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY team
    ADD CONSTRAINT team_pkey PRIMARY KEY (id);


--
-- TOC entry 2085 (class 2606 OID 17199)
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2097 (class 1259 OID 17262)
-- Name: competition_match_idx_competition; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_match_idx_competition ON competition_match USING btree (competition_id);


--
-- TOC entry 2098 (class 1259 OID 17263)
-- Name: competition_match_idx_team1; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_match_idx_team1 ON competition_match USING btree (competition_team_1_id);


--
-- TOC entry 2099 (class 1259 OID 17264)
-- Name: competition_match_idx_team2; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_match_idx_team2 ON competition_match USING btree (competition_team_2_id);


--
-- TOC entry 2108 (class 1259 OID 17270)
-- Name: competition_match_player_idx_competition_match; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_match_player_idx_competition_match ON competition_match_player USING btree (competition_match_id);


--
-- TOC entry 2109 (class 1259 OID 17269)
-- Name: competition_match_player_idx_competition_team; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_match_player_idx_competition_team ON competition_match_player USING btree (competition_team_id);


--
-- TOC entry 2110 (class 1259 OID 17271)
-- Name: competition_match_player_idx_player; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_match_player_idx_player ON competition_match_player USING btree (player_id);


--
-- TOC entry 2102 (class 1259 OID 17272)
-- Name: competition_match_result_idx_competition_match; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_match_result_idx_competition_match ON competition_match_result USING btree (competition_match_id);


--
-- TOC entry 2103 (class 1259 OID 17273)
-- Name: competition_match_result_idx_competition_team; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_match_result_idx_competition_team ON competition_match_result USING btree (competition_team_id);


--
-- TOC entry 2104 (class 1259 OID 17267)
-- Name: competition_table_idx_competition; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_table_idx_competition ON competition_table USING btree (competition_id);


--
-- TOC entry 2105 (class 1259 OID 17268)
-- Name: competition_table_idx_competition_team; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_table_idx_competition_team ON competition_table USING btree (competition_team_id);


--
-- TOC entry 2093 (class 1259 OID 17265)
-- Name: competition_team_idx_competition; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_team_idx_competition ON competition_team USING btree (competition_id);


--
-- TOC entry 2094 (class 1259 OID 17266)
-- Name: competition_team_idx_team; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX competition_team_idx_team ON competition_team USING btree (team_id);


--
-- TOC entry 2086 (class 1259 OID 17261)
-- Name: player_idx_team; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX player_idx_team ON player USING btree (team_id);


--
-- TOC entry 2115 (class 2606 OID 17279)
-- Name: competition_match_fk_competition; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match
    ADD CONSTRAINT competition_match_fk_competition FOREIGN KEY (competition_id) REFERENCES competition(id);


--
-- TOC entry 2116 (class 2606 OID 17284)
-- Name: competition_match_fk_team1; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match
    ADD CONSTRAINT competition_match_fk_team1 FOREIGN KEY (competition_team_1_id) REFERENCES competition_team(id);


--
-- TOC entry 2117 (class 2606 OID 17289)
-- Name: competition_match_fk_team2; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match
    ADD CONSTRAINT competition_match_fk_team2 FOREIGN KEY (competition_team_2_id) REFERENCES competition_team(id);


--
-- TOC entry 2123 (class 2606 OID 17324)
-- Name: competition_match_player_fk_competition_match; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match_player
    ADD CONSTRAINT competition_match_player_fk_competition_match FOREIGN KEY (competition_match_id) REFERENCES competition_match(id);


--
-- TOC entry 2122 (class 2606 OID 17319)
-- Name: competition_match_player_fk_competition_team; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match_player
    ADD CONSTRAINT competition_match_player_fk_competition_team FOREIGN KEY (competition_team_id) REFERENCES competition_team(id);


--
-- TOC entry 2124 (class 2606 OID 17329)
-- Name: competition_match_player_fk_player; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match_player
    ADD CONSTRAINT competition_match_player_fk_player FOREIGN KEY (player_id) REFERENCES player(id);


--
-- TOC entry 2118 (class 2606 OID 17294)
-- Name: competition_match_result_fk_competition_match; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match_result
    ADD CONSTRAINT competition_match_result_fk_competition_match FOREIGN KEY (competition_match_id) REFERENCES competition_match(id);


--
-- TOC entry 2119 (class 2606 OID 17299)
-- Name: competition_match_result_fk_competition_team; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_match_result
    ADD CONSTRAINT competition_match_result_fk_competition_team FOREIGN KEY (competition_team_id) REFERENCES competition_team(id);


--
-- TOC entry 2121 (class 2606 OID 17314)
-- Name: competition_table_fk_competition; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_table
    ADD CONSTRAINT competition_table_fk_competition FOREIGN KEY (competition_id) REFERENCES competition(id);


--
-- TOC entry 2120 (class 2606 OID 17309)
-- Name: competition_table_fk_competition_team; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_table
    ADD CONSTRAINT competition_table_fk_competition_team FOREIGN KEY (competition_team_id) REFERENCES competition_team(id);


--
-- TOC entry 2114 (class 2606 OID 17304)
-- Name: competition_team_fk_team; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY competition_team
    ADD CONSTRAINT competition_team_fk_team FOREIGN KEY (team_id) REFERENCES team(id);


--
-- TOC entry 2113 (class 2606 OID 17274)
-- Name: player_fk_team; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY player
    ADD CONSTRAINT player_fk_team FOREIGN KEY (team_id) REFERENCES team(id);


--
-- TOC entry 2263 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-09-01 01:58:11 CEST

--
-- PostgreSQL database dump complete
--


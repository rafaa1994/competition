<?php

namespace app\controllers;

use app\models\Competition;
use app\models\Player;
use app\models\Team;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

class TeamController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'delete', 'squads'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays competition.
     *
     * @param null $id
     * @return string
     */
    public function actionIndex($id = null)
    {
        if (!$id) {
            $model = new Team;
        } else {
            $model = Team::findOne($id);
        }

        if ($model && $model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Success.');
            $model = new Team;
        }

        $provider = new ActiveDataProvider([
            'query' => Team::find(),
        ]);
        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $provider,
        ]);
    }

    /**
     * Squads competition.
     *
     * @param null $id
     * @return string
     */
    public function actionSquads($id = null)
    {
        if (!$id) {
            $model = new Player;
        } else {
            $model = Player::findOne($id);
        }

        if ($model && $model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Success.');
            $model = new Player;
        }

        $provider = new ActiveDataProvider([
            'query' => Team::find(),
        ]);
        return $this->render('squad', [
            'model' => $model,
            'dataProvider' => $provider,
        ]);
    }


    /**
     * Displays competition.
     *
     * @param $id
     * @return string
     */
    public function actionDelete($id)
    {
        $model = Team::findOne($id);

        if ($model && $model->competitionTeams) {
            Yii::$app->session->setFlash('danger', 'Cant remove competition. It has matches.');
        } else
        if ($model && $model->delete()) {
            Yii::$app->session->setFlash('success', 'Success.');
        }

        $provider = new ActiveDataProvider([
            'query' => Team::find(),
        ]);

        return $this->render('index', [
            'model' => new Team,
            'dataProvider' => $provider,
        ]);
    }


}
<?php

namespace app\controllers;

use app\models\Competition;
use app\models\CompetitionMatch;
use app\models\CompetitionTeam;
use app\models\MatchResultForm;
use app\models\Player;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

class CompetitionMatchesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'available-players', 'available-teams', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionIndex($id = null)
    {
        if (!$id) {
            $model = new CompetitionMatch;
        } else {
            $model = CompetitionMatch::findOne($id);
        }

        if ($model && $model->load(Yii::$app->request->post()) && $model->saveMatch()) {
            Yii::$app->session->setFlash('success', 'Success.');
            $model = new CompetitionMatch();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => CompetitionMatch::find(),
        ]);

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'teams' => CompetitionTeam::find()->all(),
            'competitions' => Competition::find()->all(),
        ]);
    }


    /**
     * @return array
     */
    public function actionAvailablePlayers()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            /** @var int $id Shop.id */
            $id = Yii::$app->request->getBodyParam('depdrop_parents');
            if (is_array($id)) {
                $id = $id[0];
            }
            $output = Player::forDepDrop($id);
            return ['output' => $output, 'selected' => ''];
        }
        return ['output' => '', 'selected' => ''];
    }


    /**
     * @return array
     */
    public function actionAvailableTeams()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            /** @var int $id Shop.id */
            $id = Yii::$app->request->getBodyParam('depdrop_parents');
            if (is_array($id)) {
                $id = $id[0];
            }
            $out = CompetitionTeam::teamsForDepDrop($id);
            return ['output' => $out, 'selected' => ''];
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * @param $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $match = CompetitionMatch::findOne($id);
        if ($match) {
            $model = new MatchResultForm(['match' => $match]);
            if ($model->load(Yii::$app->request->post()) && $model->saveResult()) {
                Yii::$app->session->setFlash('success', 'Success.');
            }
            return $this->render('result', ['match' => $match, 'model' => $model]);
        }

       return $this->redirect(['index']);
    }

}
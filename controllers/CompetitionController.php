<?php

namespace app\controllers;

use app\models\Competition;
use app\models\CompetitionTeam;
use app\models\Team;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

class CompetitionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'delete', 'team', 'team-delete' ,'available-team'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays competition.
     *
     * @param null $id
     * @return string
     */
    public function actionIndex($id = null)
    {
        if (!$id) {
            $model = new Competition;
        } else {
            $model = Competition::findOne($id);
        }

        if ($model && $model->load(Yii::$app->request->post()) && $model->create()) {
            Yii::$app->session->setFlash('success', 'Success.');
            $model = new Competition;
        }

        $provider = new ActiveDataProvider([
            'query' => Competition::find(),
        ]);
        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $provider,
        ]);
    }


    /**
     * @param null $id
     * @return string
     */
    public function actionTeam($id = null)
    {
        if (!$id) {
            $model = new CompetitionTeam;
        } else {
            $model = CompetitionTeam::findOne($id);
        }

        if ($model && $model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Success.');
            $model = new CompetitionTeam;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Competition::find(),
        ]);

        return $this->render('team', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'competitions' => Competition::find()->andWhere(['>', 'date_from', Yii::$app->formatter->asTimestamp('now')])->all()
        ]);
    }

    /**
     * @return array
     */
    public function actionAvailableTeam()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            /** @var int $id Shop.id */
            $id = Yii::$app->request->getBodyParam('depdrop_parents');
            if (is_array($id)) {
                $id = $id[0];
            }
            $ids = ArrayHelper::getColumn(CompetitionTeam::findAll(['competition_id' => $id]),'team_id');
            $teams = Team::find()->andWhere(['not in', 'id', $ids])->all();
            return ['output' => $teams, 'selected' => ''];
        }
        return ['output' => '', 'selected' => ''];
    }


    /**
     * Delete competition.
     *
     * @return string
     */
    public function actionDelete($id)
    {
        $model = Competition::findOne($id);

        if ($model->competitionMatches) {
            Yii::$app->session->setFlash('danger', 'Cant remove competition. It has matches.');
        } elseif ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Success.');
        }

        $provider = new ActiveDataProvider([
            'query' => Competition::find(),
        ]);

        return $this->render('index', [
            'model' => new Competition,
            'dataProvider' => $provider,
        ]);
    }

    /**
     * Displays competition.
     *
     * @return string
     */
    public function actionTeamDelete($id)
    {
        $model = CompetitionTeam::findOne($id);

        if ($model) {
            if ($model->hasMatch()) {
                Yii::$app->session->setFlash('danger', 'Cant remove competition. It has matches.');
            } elseif ($model->delete()) {
                Yii::$app->session->setFlash('success', 'Success.');
            }
        }

        return $this->redirect(['team']);
    }


}